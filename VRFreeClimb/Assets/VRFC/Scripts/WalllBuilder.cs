﻿using UnityEngine;
using System.Collections;

public class WalllBuilder : MonoBehaviour {
    public GameObject objHold;
    public float WallX = -4.96f;
    public float StartY = 3.63f;
    public float MaxHeight;

	// Use this for initialization
	void Start () {
        float currentY = StartY;
        float currentZ = 0f;
        bool buildMore = true;

        //Loop the whole height
        while (buildMore)
        {
            currentZ += Random.Range(-2.0f, 2.0f);
            if (currentZ > 4)
                currentZ = (currentZ - 4);
            else if (currentZ < -4)
                currentZ = currentZ + 4;

            currentY += Random.Range(0.75f, 1.75f);

            //Topity top
            if (currentY > MaxHeight || currentY > MaxHeight - 1.0f)
            {
                currentY = MaxHeight - 0.125f;
                buildMore = false;
            } 

            //We're going to randomly cover this wall
            Instantiate(objHold, new Vector3(-4.96f, currentY, currentZ), Quaternion.identity);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
