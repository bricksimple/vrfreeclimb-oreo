﻿using UnityEngine;
using System.Collections;

public class HandTest : MonoBehaviour {
    private Animator animator;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>() as Animator;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void doRightGrip()
    {
        animator.Play("RightGrip");
    }

    public void doOpenHand()
    {
        animator.Play("FlatHand");
    }

    public void doLeftGrip()
    {
        animator.Play("LeftGrip");
    }
}
