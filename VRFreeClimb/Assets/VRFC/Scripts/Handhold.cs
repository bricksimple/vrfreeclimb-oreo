﻿using UnityEngine;
using System.Collections;

public class Handhold : MonoBehaviour {
    private GameObject playerObj;
    private GameObject recticleObj;

	// Use this for initialization
	void Start () {
        //We should maintain a reference to the player
        playerObj = GameObject.FindGameObjectWithTag("Player");

        //We need one for the recticle as well, because we're going to have some fun
        recticleObj = GameObject.FindGameObjectWithTag("Recticle");

        //Actual grip not visible at run time
        GetComponent<MeshRenderer>().enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void warpToHold()
    {
        if(playerObj)
            playerObj.transform.position = playerPositionForThisHold();
    }

    public void moveToHold()
    {
        //playerObj.SendMessage("SetTarget", playerPositionForThisHold());
        if(playerObj)
            playerObj.SendMessage("TakeHold", this.gameObject);
    }

    public Vector3 playerPositionForThisHold()
    {
        return new Vector3(this.gameObject.transform.position.x + 0.75f, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
    }

    public Vector3 handPositionForThisHold(int idx)
    {
        //idx = 0 left, 1 right, 2 center
        Vector3 retVect = new Vector3();
        if(idx == 0)
        {
            retVect = new Vector3(this.gameObject.transform.position.x + 0.12f, this.gameObject.transform.position.y, this.gameObject.transform.position.z - 0.25f);
        } else if (idx == 1)
        {
            retVect = new Vector3(this.gameObject.transform.position.x + 0.12f, this.gameObject.transform.position.y, this.gameObject.transform.position.z + 0.25f);
        } else
        {
            retVect  = new Vector3(this.gameObject.transform.position.x + 0.12f, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
        }

        return retVect;
    }

    public void testDistance()
    {
        Debug.Log("Distance is " + Vector3.Distance(playerPositionForThisHold(), playerObj.transform.position));
        if(playerObj)
            playerObj.SendMessage("ExamineHold", this.gameObject);
    }


    public void lookedAway()
    {
        //Let the player script know that we're no longer looking at this hold
        if(playerObj)
            playerObj.SendMessage("LookedAwayFromHold", this.gameObject);
    }
}
