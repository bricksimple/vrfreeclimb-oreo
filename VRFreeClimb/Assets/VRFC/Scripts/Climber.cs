﻿using UnityEngine;
using System.Collections;

public class Climber : MonoBehaviour {
    public Vector3 targetPosition;
    public GameObject HandLeft;
    public GameObject HandRight;
    private bool NoHandhold = true;
    public GameObject CurrentHold;
    public GameObject InitialHold;
    private Vector3 vHL;
    private Vector3 vHR;
    private float easyReach = 2.0f;
    private bool death = false;

	// Use this for initialization
	void Start () {
        targetPosition = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate()
    {   
        if(!death)
            transform.position = Vector3.Lerp(this.transform.position, targetPosition, 5.0f * Time.deltaTime);
    }

    public void ExamineHold(GameObject objHold)
    {
        if (!death)
        {
            if (NoHandhold)
            {
                if (InitialHold != null)
                    objHold = InitialHold;

                //First grab we're going there with both hands
                HandLeft.SendMessage("GrabPosition", objHold.GetComponent<Handhold>().handPositionForThisHold(0));
                HandRight.SendMessage("GrabPosition", objHold.GetComponent<Handhold>().handPositionForThisHold(1));
            }
            else
            {
                if (Vector3.Distance(this.gameObject.transform.position, objHold.GetComponent<Handhold>().playerPositionForThisHold()) > easyReach)
                {
                    //Reach for position
                    Vector3 vReach = 1.2f * Vector3.Normalize(objHold.GetComponent<Handhold>().handPositionForThisHold(2) - this.gameObject.transform.position) + this.gameObject.transform.position;

                    //Nearest Hand reaches
                    if (Vector3.Distance(HandLeft.transform.position, objHold.transform.position) < Vector3.Distance(HandRight.transform.position, objHold.transform.position))
                    {
                        HandLeft.SendMessage("ReachForPosition", vReach);
                    }
                    else
                    {
                        HandRight.SendMessage("ReachForPosition", vReach);
                    }
                }
                else
                {
                    //Determine hand to grab
                    if (Vector3.Distance(HandLeft.transform.position, objHold.transform.position) < Vector3.Distance(HandRight.transform.position, objHold.transform.position))
                    {
                        HandLeft.SendMessage("GrabPosition", objHold.GetComponent<Handhold>().handPositionForThisHold(0));
                    }
                    else
                    {
                        HandRight.SendMessage("GrabPosition", objHold.GetComponent<Handhold>().handPositionForThisHold(1));
                    }
                }
            }
        }
    }

    public void LookedAwayFromHold(GameObject objHold)
    {
        if (!NoHandhold && !death)
        {
            //Back to original positions
            HandLeft.SendMessage("GrabPosition", vHL);
            HandRight.SendMessage("GrabPosition", vHR);
        }
    }

    public void TakeHold(GameObject objHold)
    {
        float distToHold = Vector3.Distance(this.gameObject.transform.position, objHold.GetComponent<Handhold>().playerPositionForThisHold());
        //Is this any easy reach
        if (distToHold > easyReach && !NoHandhold)
        {
            //No, this might be hard
            //We roll the dice to see if we might miss it, if we do, death
            var diceBoy = Random.Range(0, distToHold);

            //Debug case
            //diceBoy = easyReach + 1;

            //Near points have pretty good odds, far points, not so much
            if(diceBoy >= (easyReach * 1.25f))
            {
                //Didn't make it
                death = true;
     
                //Destroy hands
                Destroy(HandLeft);
                Destroy(HandRight);

                //Apply the weight of the world to player and hands
                var rigidbody = this.gameObject.AddComponent<Rigidbody>();
                rigidbody.collisionDetectionMode = CollisionDetectionMode.Continuous;

                //We're going to swing for the fences and try to reach our target
                rigidbody.AddForce(250 *((Vector3.Normalize(objHold.GetComponent<Handhold>().playerPositionForThisHold() - this.gameObject.transform.position) + this.gameObject.transform.position) - this.gameObject.transform.position));

                return;
            }

        }

        //Handle start-up
        if (InitialHold != null && NoHandhold)
            objHold = InitialHold;

        //One shot for init
        NoHandhold = false;

        //Yep, let's do it
        //Move us to where we need to be
        targetPosition = objHold.GetComponent<Handhold>().playerPositionForThisHold();

        //Hands in place
        vHL = objHold.GetComponent<Handhold>().handPositionForThisHold(0);
        vHR = objHold.GetComponent<Handhold>().handPositionForThisHold(1);
        HandLeft.SendMessage("GrabPosition", vHL);
        HandRight.SendMessage("GrabPosition", vHR);

        //Keep a reference for this
        CurrentHold = objHold;
    }

    private void SetTarget(Vector3 newTarget)
    {
        NoHandhold = false;

        targetPosition = newTarget;

        Debug.Log("New Position Set");
    }
}
