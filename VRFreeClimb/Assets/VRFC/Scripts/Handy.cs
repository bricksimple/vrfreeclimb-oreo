﻿using UnityEngine;
using System.Collections;

public class Handy : MonoBehaviour {
    public Vector3 targetPosition;
    public bool isLefty = false;
    private Animator animator;
    private int grip = Animator.StringToHash("Grip");
    private int horns = Animator.StringToHash("Horns");
    private int nervous = Animator.StringToHash("Nervous");
    private bool holding = false;
    private bool moveToGrab = false;
    private bool bDontUpdate = false;

    // Use this for initialization
    void Start () {
        GameObject hideThumb;
        if (isLefty)
        {
            //Hide the right thumb
            hideThumb = this.transform.Find("RightThumb/T0").gameObject;
        } else
        {
            //Hide the left thumb
            hideThumb = this.transform.Find("LeftThumb/T1").gameObject;
        }

        //Hide it
        hideThumb.GetComponent<MeshRenderer>().enabled = false;

        //Stash aside the animator
        animator = GetComponent<Animator>() as Animator;

        //Get us ready
        //animator.Play("HandyReady");

        //Setup other variables
        targetPosition = this.transform.position;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (!bDontUpdate)
        {
            transform.position = Vector3.Lerp(this.transform.position, targetPosition, 8.0f * Time.deltaTime);

            if (Vector3.Distance(transform.position, targetPosition) < 0.1f)
            {
                if (moveToGrab)
                {
                    showGrip();
                }
                else
                {

                }
            }
        }
    }

    public void showHorns()
    {
        animator.SetBool(horns, true);
    }

    public void showNervous()
    {
        animator.SetBool(grip, false);
        animator.SetBool(horns, false);
        animator.SetBool(nervous, true);
    }

    public void showDeath()
    {
        bDontUpdate = true;
        animator.SetBool(grip, false);
        animator.SetBool(horns, false);
        animator.SetBool(nervous, false);
    }

    public void showDefault()
    {
        bDontUpdate = false;
        animator.SetBool(grip, false);
        animator.SetBool(horns, false);
        animator.SetBool(nervous, false);
    }

    public void showGrip()
    {
        animator.SetBool(nervous, false);
        animator.SetBool(grip, true);
        holding = true;
    }

    public void GrabPosition(Vector3 grabThis)
    {
        showDefault();
        moveToGrab = true;
        targetPosition = grabThis;
    }

    public void ReachForPosition(Vector3 reachThis)
    {
        showNervous();
        moveToGrab = false;
        targetPosition = reachThis;
    }

    public bool HasGripped()
    {
        return holding;
    }
}
