﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class KillBox : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter()
    {
        var sceneName = SceneManager.GetActiveScene().name;
        Debug.Log("Rock Bottom");

        // Obsolete in Unity 2018
        //SceneManager.UnloadScene(sceneName);

        //Restart the level
        SceneManager.LoadScene(sceneName);
    }
}
