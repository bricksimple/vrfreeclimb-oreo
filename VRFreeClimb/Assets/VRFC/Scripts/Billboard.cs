﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Billboard : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnPointerDown()
    {
        var sceneName = SceneManager.GetActiveScene().name;
        Debug.Log("Restart");

        // Obsolete in Unity 2018
        //UnloadScene(sceneName);

        //Restart the level
        SceneManager.LoadScene(sceneName);
    }
}
